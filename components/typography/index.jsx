import Styled from "@emotion/styled";
import css from "@styled-system/css";

const H1 = Styled("h1")(
  css({
    fontFamily: "Josefin Sans, sans-serif",
    fontWeight: 700,
    fontSize: [36, 48, 68],
    lineHeight: ["43px", "56px", "96px"],
    letterSpacing: [-0.5625, -0.5625, -1.09375],
    textTransform: "uppercase",
    color: "#fff",
    textShadow: "2px 2px 4px rgba(0, 0, 0, 0.25)",
    maxWidth: [344, 344, 729],
  })
);

export { H1 };
