import GeneralForm from "./form";
import { DesktopWrapper, MobileWrapper } from "./styles";

export const DesktopForm = () => {
  return (
    <DesktopWrapper>
      <GeneralForm />
    </DesktopWrapper>
  );
};

export const MobileForm = () => {
  return (
    <MobileWrapper id="form">
      <GeneralForm />
    </MobileWrapper>
  );
};
