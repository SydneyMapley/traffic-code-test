import React from "react";

import {
  Wrapper,
  AccentBar,
  Container,
  LogoBox,
  NavItems,
  Item,
} from "./styles";

const Navigation = () => {
  return (
    <Wrapper>
      <AccentBar />
      <Container>
        <LogoBox>
          <img src="/images/eliston_logo.svg" />
        </LogoBox>
        <NavItems>
          <Item href="#">
            <img src="/glyphs/location.svg" />
            <span>VISIT OUR SALES CENTRE</span>
          </Item>
          <Item href="#">
            <img src="/glyphs/phone.svg" />
            <span>1300 354 786</span>
          </Item>
        </NavItems>
      </Container>
    </Wrapper>
  );
};

export default Navigation;
