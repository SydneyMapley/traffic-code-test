import "../styles/globals.css";
import "swiper/swiper.scss";
import "swiper/components/effect-fade/effect-fade.scss";
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
