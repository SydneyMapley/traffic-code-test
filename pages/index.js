import React from "react";
import Head from "next/head";
import Navigation from "../components/navigation";
import { H1 } from "../components/typography";
import { Main, Container, PageWrapper, Hero } from "../components/layout";
import { Button } from "../components/common/button";
import Slider from "../components/Slider";
import { DesktopForm, MobileForm } from "../components/form";

export default function Home() {
  return (
    <PageWrapper>
      <Head>
        <title>Traffic Tech Test</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navigation />
      <Main>
        <Hero>
          <Container>
            <div>
              <H1>TWO STUNNING NEW TOWNHOME RELEASES LAUNCHING EARLY 2021</H1>
              <Button href="#form">Register</Button>
            </div>
            <div>
              <DesktopForm />
            </div>
          </Container>
          <Slider />
        </Hero>
        <MobileForm />
      </Main>
    </PageWrapper>
  );
}
